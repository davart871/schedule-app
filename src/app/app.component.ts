import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private translate: TranslateService,
    public platform: Platform
  ) {

    this.initializeApp();
  }

  async initializeApp() {

    this.platform.ready().then(() => {

    });
    
  }


  
}
