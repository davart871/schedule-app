/** Cette classe définit l'objet User (res.user) **/

export class User {

  public id: number;
  public display_name: string;
  public name: string;
  public login: string;
  public login_date: string;
  public image_url: string;
  public mobile: string;
  public email: string;
  public city: string;
  public signature: string;
  public tz: {id: string; titre: string};
  public notification_type: {id: string; titre: string};
  public lang: {id: string; titre: string};
  public partner_id: {id: number; name: string};
  public country_id: {id: number; name: string};
  public state_id: {id: number; name: string};
  public sale_team_id: {id: number; name: string};
  public company_id: {id: number; name: string};
  public currency_id: {id: number; name: string};
  public meeting_ids: number[];
  public invoice_ids: number[];
  public opportunity_ids: number[];
  public sale_order_ids: number[];
  public groups_id: number[];
  public signup_expiration: string;
  public new_password?: string;
  public tz_offset: string; //activities done target


  constructor(serverJSON?: any) {
    // this.popLang = objLang;
    if(serverJSON!==undefined)
      this.setUser(serverJSON);
    else
      this.initUser();
  } 

  private getIdTabs(liste){

    let tab = [];
    for (var i = 0; i < liste.length; i++) {
        tab.push(liste[i].me);
    }

    return tab;
  }
  
  /** 
   * Cette fonction retourne le libelle de la notification
   * en fonction de l'id
   * @param id string, l'id du titre
   * @return string 
   **/
  getTitreById(id, tab){

    for (let i = 0; i < tab.length; i++) {
      if(tab[i].id==id)
        return tab[i].text;
    }

  }

  /** Cette fonction permet de définir 
   * les valeurs des champs
   * @param data JSONObject, il s'agit des données JSON du serveur
   *
   ***/
  setUser(data : any){
    
    this.id = data.me.id.me;

    if(data.me.display_name==undefined || !data.me.display_name.me)
      this.display_name = "";
    else
      this.display_name = data.me.display_name.me;

    if(data.me.name==undefined || !data.me.name.me)
      this.name = "";
    else
      this.name = data.me.name.me;
    
    if(data.me.login_date==undefined || !data.me.login_date.me)
      this.login_date = "";
    else
      this.login_date = data.me.login_date.me;
    
    if(data.me.city==undefined || !data.me.city.me)
      this.city = "";
    else
      this.city = data.me.city.me;
    
    if(data.me.signup_expiration==undefined || !data.me.signup_expiration.me)
      this.signup_expiration = "";
    else
      this.signup_expiration = data.me.signup_expiration.me;
    
    if(data.me.mobile==undefined || !data.me.mobile.me)
      this.mobile = "";
    else
      this.mobile = data.me.mobile.me;

    if(data.me.login==undefined || !data.me.login.me)
      this.login = "";
    else
      this.login = data.me.login.me;
    
    if(data.me.tz_offset==undefined || !data.me.tz_offset.me)
      this.tz_offset = "";
    else
      this.tz_offset = data.me.tz_offset.me;

    if(!data.me.email.me || data.me.email===undefined)
      this.email = "";
    else
      this.email = data.me.email.me;

    if(data.me.tz===undefined || !data.me.tz.me)
      this.tz = { id: '', titre: 'Aucune'};
    else
      this.tz = { id: data.me.tz.me, titre:"" };

    if(data.me.signature===undefined || !data.me.signature.me)
      this.signature = "";
    else
      this.signature = String(data.me.signature.me).replace(/<[^>]+>/gm, '');

    if(data.me.image_url===undefined || !data.me.image_url.me)
      this.image_url = "assets/imgs/person.jpg";
    else
      this.image_url = data.me.image_url.me;
  
    
    //ON attribut des valeurs aux restes des propriétés
    if(data.me.notification_type==undefined || !data.me.notification_type.me)
      this.notification_type = { id: 'none', titre: 'Jamais'};
    else 
      this.notification_type = { id: data.me.notification_type.me, titre: ""};

    if(data.me.lang==undefined || !data.me.lang.me)
      this.lang = { id: '', titre: 'Aucune'};
    else  
      this.lang = { id: data.me.lang.me, titre: "" };

    if(data.me.partner_id==undefined || !data.me.partner_id.me)
      this.partner_id = { id: 0, name: ''};
    else  
      this.partner_id = {
        id: data.me.partner_id.me[0].me,
        name: data.me.partner_id.me[1].me,
      };
    
    if(data.me.country_id==undefined || !data.me.country_id.me)
      this.country_id = { id: 0, name: ''};
    else  
      this.country_id = { id: data.me.country_id.me[0].me, name: data.me.country_id.me[1].me};
    
    if(data.me.state_id==undefined || !data.me.state_id.me)
      this.state_id = { id: 0, name: ''};
    else  
      this.state_id = { id: data.me.state_id.me[0].me, name: data.me.state_id.me[1].me};
    
    if(data.me.currency_id==undefined || !data.me.currency_id.me)
      this.currency_id = { id: 0, name: ''};
    else  
      this.currency_id = {
        id: data.me.currency_id.me[0].me,
        name: data.me.currency_id.me[1].me,
      };
    
    if(data.me.company_id==undefined || !data.me.company_id.me)
      this.company_id = { id: 0, name: ''};
    else  
      this.company_id = {
        id: data.me.company_id.me[0].me,
        name: data.me.company_id.me[1].me,
      };

    if(data.me.sale_team_id==undefined || !data.me.sale_team_id.me)
      this.sale_team_id = { id: 0, name: ''};
    else  
      this.sale_team_id = {
        id: data.me.sale_team_id.me[0].me,
        name: data.me.sale_team_id.me[1].me,
      };

    if(data.me.meeting_ids==undefined || !data.me.meeting_ids.me || data.me.meeting_ids.me.length==0)
      this.meeting_ids = [];
    else
      this.meeting_ids = this.getIdTabs(data.me.meeting_ids.me);
    
    if(data.me.groups_id==undefined || !data.me.groups_id.me || data.me.groups_id.me.length==0)
      this.groups_id = [];
    else
      this.groups_id = this.getIdTabs(data.me.groups_id.me);
    
    if(data.me.invoice_ids==undefined || !data.me.invoice_ids.me || data.me.invoice_ids.me.length==0)
      this.invoice_ids = [];
    else
      this.invoice_ids = this.getIdTabs(data.me.invoice_ids.me);
    
    if(data.me.opportunity_ids==undefined || !data.me.opportunity_ids.me || data.me.opportunity_ids.me.length==0)
      this.opportunity_ids = [];
    else
      this.opportunity_ids = this.getIdTabs(data.me.opportunity_ids.me);
    
    if(data.me.sale_order_ids==undefined || !data.me.sale_order_ids.me || data.me.sale_order_ids.me.length==0)
      this.sale_order_ids = [];
    else
      this.sale_order_ids = this.getIdTabs(data.me.sale_order_ids.me);
    
  }

  //initialisation de l'objet User
  initUser(){

    this.id = 0;
    this.tz_offset = "";
    this.display_name = "";
    this.name = "";
    this.mobile = "";
    this.login = "";
    this.email = "";
    this.city = "";
    this.signup_expiration = "";
    this.tz = { id: '', titre: 'Aucune'};
    this.signature = "";
    this.image_url = "assets/imgs/person.jpg";
    this.notification_type = { id: 'none', titre: 'Jamais'};
    this.lang = { id: '', titre: 'Aucune'};
    this.partner_id = { id: 0, name: ''};
    this.sale_team_id = { id: 0, name: ''};
    this.company_id = { id: 0, name: ''};
    this.currency_id = { id: 0, name: ''};
    this.country_id = { id: 0, name: ''};
    this.state_id = { id: 0, name: ''};
    this.meeting_ids = [];
    this.invoice_ids = [];
    this.groups_id = [];
    this.opportunity_ids = [];
    this.sale_order_ids = [];
    
  }

}
